#define __unsigned_chars__ 1	/* Predefined */
#define __DATE__ "Jan 22 2019"	/* Predefined */
#define __TIME__ "12:32:48"	/* Predefined */
#define __STDC__ 1	/* Predefined */
#define __STDC_VERSION__ 199409L	/* Predefined */
#define __edg_front_end__ 1	/* Predefined */
#define __EDG_VERSION__ 303	/* Predefined */
#define __TI_COMPILER_VERSION__ 4001005	/* Predefined */
#define __COMPILER_VERSION__ 4001005	/* Predefined */
#define __MSP430__ 1	/* Predefined */
#define __SIZE_T_TYPE__ unsigned	/* Predefined */
#define __PTRDIFF_T_TYPE__ int	/* Predefined */
#define __WCHAR_T_TYPE__ unsigned int	/* Predefined */
#define __little_endian__ 1	/* Predefined */
#define __TI_STRICT_ANSI_MODE__ 1	/* Predefined */
#define __TI_WCHAR_T_BITS__ 16	/* Predefined */
#define __TI_GNU_ATTRIBUTE_SUPPORT__ 0	/* Predefined */
#define __TI_STRICT_FP_MODE__ 1	/* Predefined */
#define _INLINE 1
#define MAX_HOPS 3
#define MAX_HOPS_FROM_AP 1
#define MAX_NWK_PAYLOAD 9
#define MAX_APP_PAYLOAD 10
#define DEFAULT_LINK_TOKEN 0x01020304
#define DEFAULT_JOIN_TOKEN 0x05060708
#define EXTENDED_API 1
#define SW_TIMER 1
#define NUM_CONNECTIONS 8
#define SIZE_INFRAME_Q 6
#define SIZE_OUTFRAME_Q 2
#define THIS_DEVICE_ADDRESS {0x78, 0x56, 0x34, 0x12}
#define ACCESS_POINT 1
#define AP_IS_DATA_HUB 1
#define NUM_STORE_AND_FWD_CLIENTS 3
#define STARTUP_JOINCONTEXT_ON 1
#define __MSP430F2274__ 1
#define MRFI_CC2500 1
#define _OPTIMIZE_FOR_SPACE 1
