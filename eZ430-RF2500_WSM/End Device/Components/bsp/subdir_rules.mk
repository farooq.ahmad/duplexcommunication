################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
Components/bsp/bsp.obj: C:/Users/Engineer/ccs\ project/ccs2/Code/Components/bsp/bsp.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv8/tools/compiler/msp430_4.1.5/bin/cl430" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_nwk_config.dat" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_config_ED.dat"  -vmsp --abi=coffabi -O3 --opt_for_speed=0 -g --include_path="C:/ti/ccsv8/ccs_base/msp430/include" --include_path="C:/ti/ccsv8/ccs_base/msp430/include" --include_path="C:/ti/ccsv8/tools/compiler/msp430_4.1.5/include" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/boards/EZ430RF" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/drivers" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/mrfi" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk_applications" --define=__MSP430F2274__ --define=MRFI_CC2500 --diag_warning=225 --printf_support=minimal --preproc_with_compile --preproc_dependency="Components/bsp/bsp.d_raw" --obj_directory="Components/bsp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '


