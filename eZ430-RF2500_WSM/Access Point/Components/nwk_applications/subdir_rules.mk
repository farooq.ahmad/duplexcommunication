################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
Components/nwk_applications/nwk_freq.obj: C:/Users/Engineer/ccs\ project/ccs2/Code/Components/simpliciti/nwk_applications/nwk_freq.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv8/tools/compiler/msp430_4.1.5/bin/cl430" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_nwk_config.dat" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_config_AP.dat"  -vmsp -g -O3 --opt_for_speed=0 --define=__MSP430F2274__ --define=MRFI_CC2500 --include_path="C:/ti/ccsv8/ccs_base/msp430/include" --include_path="C:/ti/ccsv8/tools/compiler/msp430_4.1.5/include" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/boards/EZ430RF" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/drivers" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/mrfi" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk_applications" --diag_warning=225 --printf_support=minimal --preproc_with_compile --preproc_dependency="Components/nwk_applications/nwk_freq.d_raw" --obj_directory="Components/nwk_applications" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

Components/nwk_applications/nwk_ioctl.obj: C:/Users/Engineer/ccs\ project/ccs2/Code/Components/simpliciti/nwk_applications/nwk_ioctl.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv8/tools/compiler/msp430_4.1.5/bin/cl430" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_nwk_config.dat" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_config_AP.dat"  -vmsp -g -O3 --opt_for_speed=0 --define=__MSP430F2274__ --define=MRFI_CC2500 --include_path="C:/ti/ccsv8/ccs_base/msp430/include" --include_path="C:/ti/ccsv8/tools/compiler/msp430_4.1.5/include" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/boards/EZ430RF" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/drivers" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/mrfi" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk_applications" --diag_warning=225 --printf_support=minimal --preproc_with_compile --preproc_dependency="Components/nwk_applications/nwk_ioctl.d_raw" --obj_directory="Components/nwk_applications" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

Components/nwk_applications/nwk_join.obj: C:/Users/Engineer/ccs\ project/ccs2/Code/Components/simpliciti/nwk_applications/nwk_join.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv8/tools/compiler/msp430_4.1.5/bin/cl430" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_nwk_config.dat" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_config_AP.dat"  -vmsp -g -O3 --opt_for_speed=0 --define=__MSP430F2274__ --define=MRFI_CC2500 --include_path="C:/ti/ccsv8/ccs_base/msp430/include" --include_path="C:/ti/ccsv8/tools/compiler/msp430_4.1.5/include" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/boards/EZ430RF" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/drivers" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/mrfi" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk_applications" --diag_warning=225 --printf_support=minimal --preproc_with_compile --preproc_dependency="Components/nwk_applications/nwk_join.d_raw" --obj_directory="Components/nwk_applications" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

Components/nwk_applications/nwk_link.obj: C:/Users/Engineer/ccs\ project/ccs2/Code/Components/simpliciti/nwk_applications/nwk_link.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv8/tools/compiler/msp430_4.1.5/bin/cl430" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_nwk_config.dat" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_config_AP.dat"  -vmsp -g -O3 --opt_for_speed=0 --define=__MSP430F2274__ --define=MRFI_CC2500 --include_path="C:/ti/ccsv8/ccs_base/msp430/include" --include_path="C:/ti/ccsv8/tools/compiler/msp430_4.1.5/include" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/boards/EZ430RF" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/drivers" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/mrfi" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk_applications" --diag_warning=225 --printf_support=minimal --preproc_with_compile --preproc_dependency="Components/nwk_applications/nwk_link.d_raw" --obj_directory="Components/nwk_applications" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

Components/nwk_applications/nwk_mgmt.obj: C:/Users/Engineer/ccs\ project/ccs2/Code/Components/simpliciti/nwk_applications/nwk_mgmt.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv8/tools/compiler/msp430_4.1.5/bin/cl430" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_nwk_config.dat" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_config_AP.dat"  -vmsp -g -O3 --opt_for_speed=0 --define=__MSP430F2274__ --define=MRFI_CC2500 --include_path="C:/ti/ccsv8/ccs_base/msp430/include" --include_path="C:/ti/ccsv8/tools/compiler/msp430_4.1.5/include" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/boards/EZ430RF" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/drivers" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/mrfi" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk_applications" --diag_warning=225 --printf_support=minimal --preproc_with_compile --preproc_dependency="Components/nwk_applications/nwk_mgmt.d_raw" --obj_directory="Components/nwk_applications" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

Components/nwk_applications/nwk_ping.obj: C:/Users/Engineer/ccs\ project/ccs2/Code/Components/simpliciti/nwk_applications/nwk_ping.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv8/tools/compiler/msp430_4.1.5/bin/cl430" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_nwk_config.dat" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_config_AP.dat"  -vmsp -g -O3 --opt_for_speed=0 --define=__MSP430F2274__ --define=MRFI_CC2500 --include_path="C:/ti/ccsv8/ccs_base/msp430/include" --include_path="C:/ti/ccsv8/tools/compiler/msp430_4.1.5/include" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/boards/EZ430RF" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/drivers" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/mrfi" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk_applications" --diag_warning=225 --printf_support=minimal --preproc_with_compile --preproc_dependency="Components/nwk_applications/nwk_ping.d_raw" --obj_directory="Components/nwk_applications" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

Components/nwk_applications/nwk_security.obj: C:/Users/Engineer/ccs\ project/ccs2/Code/Components/simpliciti/nwk_applications/nwk_security.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv8/tools/compiler/msp430_4.1.5/bin/cl430" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_nwk_config.dat" --cmd_file="C:/Users/Engineer/ccs project/ccs2/eZ430-RF2500_WSM/../Code/Configuration/smpl_config_AP.dat"  -vmsp -g -O3 --opt_for_speed=0 --define=__MSP430F2274__ --define=MRFI_CC2500 --include_path="C:/ti/ccsv8/ccs_base/msp430/include" --include_path="C:/ti/ccsv8/tools/compiler/msp430_4.1.5/include" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/boards/EZ430RF" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/bsp/drivers" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/mrfi" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk" --include_path="C:/Users/Engineer/ccs project/ccs2/Code/Components/simpliciti/nwk_applications" --diag_warning=225 --printf_support=minimal --preproc_with_compile --preproc_dependency="Components/nwk_applications/nwk_security.d_raw" --obj_directory="Components/nwk_applications" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '


